﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class ButtonsController : MonoBehaviour
{
    [SerializeField]
    private Button _errorCoinsOkButton;

    [SerializeField]
    private GameObject _errorCoinsBg;
    private void Start()
    {
        _errorCoinsOkButton.onClick.AddListener(ErrorCoinsOkClick);

        void ErrorCoinsOkClick()
        {
            _errorCoinsBg.SetActive(false);
        }
    }

}
