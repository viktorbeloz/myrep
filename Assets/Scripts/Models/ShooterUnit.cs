﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Models
{
    class ShooterUnit: Unit
    {

        public ShooterUnit()
        {
            dataUnit = Resources.Load<DataUnit>("Units/Shooter");
            
        }
    }
}
