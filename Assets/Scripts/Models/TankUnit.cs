﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Models
{
    class TankUnit:  Unit
    {

        public TankUnit()
        {
            dataUnit = Resources.Load<DataUnit>("Units/Tank");
        }

    }
}
