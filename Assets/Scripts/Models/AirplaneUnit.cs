﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Models
{
    class AirplaneUnit: Unit
    {
        public AirplaneUnit()
        {
            dataUnit = Resources.Load<DataUnit>("Units/Airplane");
        }

    }
}
