﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Models
{
    [Serializable]
    public class Unit
    {
        public static int Count = 0;

        public int id;
        public DataUnit dataUnit;
        public Unit()
        {           
            id = ++Count;
        }

    }
}
