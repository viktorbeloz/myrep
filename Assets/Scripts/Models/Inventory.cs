﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Models
{
    [Serializable]
    public class Inventory
    {
        public int coins;
        public List<Unit> units = new List<Unit>();        

        public bool Purchase(Unit unit)
        {
            if((coins-unit.dataUnit.price)<0)
            {
                return false;
            }           
            return true;
        }


    }
}
