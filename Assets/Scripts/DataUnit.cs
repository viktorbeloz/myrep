﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "UnitData")]
public class DataUnit : ScriptableObject
{
    public string title;
    public int price;
    public string imageUrl;

    public int attack;
    public int protection;
    public int luck;
    public int speed;

    public int quantity;
}
